# -*- encoding: binary -*-
# Placed in the public domain by Eric Wong <normalperson@yhbt.net>

require 'rack/utils'

class StupidCache
  class SerializableBody
    def initialize(app)
      @app = app
    end

    def call(env)
      code, headers, body = @app.call(env)
      headers = Rack::Utils::HeaderHash.new(headers)

      new_body = ""
      body.each { |part| new_body << part }
      body.respond_to?(:close) and body.close rescue nil
      body = [ new_body ]
      headers.delete("Transfer-Encoding")
      headers["Content-Length"] = new_body.size.to_s

      [ code.to_i , headers.to_hash, body ]
    end
  end
end

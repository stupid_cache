# Placed in the public domain by Eric Wong <normalperson@yhbt.net>
require 'rack'
require 'rack/request'

class StupidCache
  VERSION = '0.1.0'

  def initialize(app, opts)
    @app = app
    @cache = opts[:cache]
    @expiry = opts[:expiry] || 60
    @limit = opts[:limit] || 250 # memcached limit
  end

  def call(env)
    return @app.call(env) unless env["REQUEST_METHOD"] == "GET"
    key = Rack::Request.new(env).url

    # Don't allow keys to get too long.
    return @app.call(env) if key.size > @limit

    # return value
    begin
      response = @cache.get(key)
      unless response
        response = @app.call(env)

        # ensure any memcache failures are non-fatal
        begin
          @cache.set(key, response, @expiry)
        rescue => err
          env['rack.errors'].write("#{self.class} error: #{err.inspect}\n")
        end
      end
      response
    rescue => err
      env['rack.errors'].write("#{self.class} error: #{err.inspect}\n")
      @app.call(env)
    end
  end
end

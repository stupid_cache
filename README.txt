= stupid_cache: Stupidly simple memcached middleware for Rack

* http://yhbt.net/stupid_cache/

== Description

Stupidly simple Rack middleware for caching responses with Memcached.
It should be easy to hack and customize for your own caching/expiry
policies (which may not be standards-compliant, but perhaps faster
and more effective).

Use Rack::Cache if you want standards-compliant caching and expiry.

== Features

* Simple source code, easy to hack and adapt to your application

* Supports both the pure-Ruby memcache-client or libmemcached+memcached

* Implementation-specific options for both memcache-client and
  memcached+libmemcached are exposed directly to the user

* Graceful fallback in case of all memcached exceptions

== Synopsis

In the Rackup configuration file for your Rack application:

Using memcache-client:

  use StupidCache, :cache => MemCache.new(%w(10.0.0.1 10.0.0.2))

Using memcached+libmemcached:

  use StupidCache, :cache => Memcached.new(%w(10.0.0.1 10.0.0.2))

== Requirements

* memcache-client _or_ memcached RubyGem

Your web server or cluster should also have at least one memcached instance
running.

Only the #set(key, value, expiry) and #get(key) methods are used by
StupidCache, so using or writing another compatible interface should be
possible with minimal effort.

== Install

You may install it via Rubygems on Rubyforge:

  gem install stupid_cache

You can get the latest source via git from the following locations
(these versions may not be stable):

  git://yhbt.net/stupid_cache.git
  https://git.yhbt.net/stupid_cache.git

You may also browse the source and download tarballs via cgit:

* https://yhbt.net/stupid_cache.git/

== License

Stupid Cache is Free Software and placed into the public domain by
Eric Wong <normalperson@yhbt.net>.  No rights reserved, there are
no copyrights nor any restrictions on use.  The included setup.rb
retains its original license, LGPL 2.1.

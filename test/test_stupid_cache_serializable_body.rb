require "test/unit"
require "stupid_cache/serializable_body"
require "stringio"

class TestStupidCacheSerializableBody < Test::Unit::TestCase

  def test_basic
    body = StringIO.new("a\nb\nc\n")
    app = lambda { |env| [ 200, {'Content-Length' => body.size.to_s }, body ] }
    env = { 'REQUEST_METHOD' => 'GET', 'REQUEST_URI' => '/hello' }
    app = StupidCache::SerializableBody.new(app)
    response = app.call(env)
    expect = [ 200, { 'Content-Length' => '6'}, [ "a\nb\nc\n" ] ]
    assert_equal expect, response
  end
end

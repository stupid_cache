require "test/unit"
require "stupid_cache"
require "stringio"

class TestStupidCache < Test::Unit::TestCase

  def test_basic
    result = [ 200, {}, [] ]
    app = lambda { |env| Marshal.load(Marshal.dump(result)) }
    env = { 'REQUEST_METHOD' => 'GET', 'REQUEST_URI' => '/hello' }
    cache = mock_cache
    sc = StupidCache.new(app, :cache => cache)
    response = sc.call(env)
    assert_equal result, response

    hash = cache.instance_variable_get('@hash')
    assert hash
    assert_equal 1, hash.size
    assert_equal Marshal.dump(result), hash.values.first
  end

  def test_cannot_marshal
    result = [ 200, {}, $stdin ]
    errors = StringIO.new
    app = lambda { |env| [ 200, {}, $stdin ] }
    env = {
      'REQUEST_METHOD' => 'GET',
      'REQUEST_URI' => '/hello',
      'rack.errors' => errors,
    }
    cache = mock_cache
    sc = StupidCache.new(app, :cache => cache)
    response = sc.call(env)
    assert_equal result, response

    hash = cache.instance_variable_get('@hash')
    assert hash
    assert_equal 0, hash.size
    assert_match(/TypeError/, errors.string)
  end

private

  def mock_cache
    mock = Object.new

    def mock.set(key, value, expiry = 0, raw = false)
      @hash ||= {}
      @hash[key] = raw ? value : Marshal.dump(value)
    end

    def mock.get(key, raw = false)
      result = (@hash ||= {})[key] or return nil
      raw ? result : Marshal.load(result)
    end

    mock
  end

end
